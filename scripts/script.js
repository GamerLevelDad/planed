const image = document.getElementById('image');
const guessInput = document.getElementById('guess-input');
const guessButton = document.getElementById('guess-button');
const guessHistory = document.getElementById('guess-history');
const guessesRemaining = document.getElementById('guesses-remaining');

// Initialize the game
const airplaneImages = [
  "images/airplane1.jpg","images/airplane2.jpg","images/airplane3.jpg","images/airplane4.jpg","images/airplane5.jpg"
];



// Set the number of guesses remaining
guessesRemaining.innerHTML = 5;
var currentAirplanePicture = 1;

// Set the current airplane image
const currentAirplane = airplaneImages[guessesRemaining.innerHTML - 1];
//image.src = currentAirplane;


const correctAnswer = `F-15`;

image1.src = airplaneImages[0];
image2.src = airplaneImages[1];
image3.src = airplaneImages[2];
image4.src = airplaneImages[3];
image5.src = airplaneImages[4];
hideAllImages();
hideAllButtons();
showImage(currentAirplanePicture);
showButton(currentAirplanePicture);

loadGameState();
// Handle the guess button click
guessButton.addEventListener('click', () => {
  const guess = guessInput.value;

  // Check if the guess is correct
  if (guess === correctAnswer) {
    // Game over, user wins!
	guessHistory.innerHTML += `<li>${guess}</li>`;
    guessHistory.innerHTML += `<li>The correct answer is `+correctAnswer+`</li>`;
	guessButton.disabled = true;
	showAllButtons();
	saveGameState();
    return;
  }
  
  // Otherwise, update the game state
  guessesRemaining.innerHTML = guessesRemaining.innerHTML - 1;

  // Add the guess to the history
  guessHistory.innerHTML += `<li>${guess}</li>`;
  if (guessesRemaining.innerHTML == 0) {
	  // Game Over, user loses
	  guessHistory.innerHTML += `<li>The correct answer is `+correctAnswer+`</li>`;
	  guessButton.disabled = true;
	  return;
  }
  // Update the image
  //image.src = airplaneImages[guessesRemaining.innerHTML - 1];
  currentAirplanePicture += 1;
  showImage(currentAirplanePicture);
  showButton(currentAirplanePicture);
  saveGameState();
});

// Highlight correct guesses green and incorrect guesses red
guessHistory.addEventListener('mouseover', (event) => {
  const guess = event.target.textContent;

  if (guess === correctAnswer) {
    event.target.style.color = 'green';
  } else {
    event.target.style.color = 'red';
  }
});

guessHistory.addEventListener('mouseout', (event) => {
  event.target.style.color = '';
});



function hideAllImages() {
  document.querySelectorAll("#image-carousel img").forEach(image => {
    image.style.display = `none`;
  });
}

function hideAllButtons() {
  document.querySelectorAll("#image-carousel button").forEach(button => {
    button.style.display = "none";
  });
}

function showImage(imageNumber) {
  // Hide all images
  document.querySelectorAll("#image-carousel img").forEach(image => {
    image.style.display = "none";
  });

  // Show the selected image
  document.querySelector("#image" + imageNumber).style.display = "block";
}

function showButton(buttonNumber) {
  // Show the selected image
  document.querySelector("#button" + buttonNumber).style.display = "inline";
}

function showAllButtons() {
	  document.querySelectorAll("#image-carousel button").forEach(button => {
    button.style.display = "inline";
  });

}

function saveGameState() {
  const state = {
    currentAirplanePicture,
    guessesRemaining,
    guessHistory
  };

  // Serialize the state.
  const serializedState = JSON.stringify(state);

  // Save the serialized state to local storage.
  localStorage.setItem('gameState', serializedState);
}

function loadGameState() {
  // Load the serialized state from local storage.
  const serializedState = localStorage.getItem('gameState');

  // Deserialize the state.
  const state = JSON.parse(serializedState);

  return state;
}